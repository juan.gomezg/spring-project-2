package com.example.demo;

import java.util.ArrayList;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}
	
	@GetMapping("/students/{id}")
	String students(@PathVariable int id) {
		ArrayList<String> students = new ArrayList<String>();
		students.add("Juan");
		students.add("Luis");
		students.add("Yeslie");
		students.add("Cristhian");
		students.add("Nicolas");
		
		return students.get(id);

	}
	

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}